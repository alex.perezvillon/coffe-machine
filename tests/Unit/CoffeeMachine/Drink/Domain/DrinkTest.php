<?php

namespace GetWith\CoffeeMachine\Tests\Unit\CoffeeMachine\Drink\Domain;

use GetWith\CoffeeMachine\CoffeeMachine\Drink\Domain\Drink;
use GetWith\CoffeeMachine\CoffeeMachine\Drink\Domain\ValueObject\DrinkExtraHot;
use GetWith\CoffeeMachine\CoffeeMachine\Drink\Domain\ValueObject\DrinkPrice;
use GetWith\CoffeeMachine\CoffeeMachine\Drink\Domain\ValueObject\DrinkSugar;
use GetWith\CoffeeMachine\CoffeeMachine\Drink\Domain\ValueObject\DrinkType;
use GetWith\CoffeeMachine\Tests\Assets\Drink\DrinkAsset;
use PHPUnit\Framework\TestCase;

class DrinkTest extends TestCase
{

    protected function setUp(): void
    {
        parent::setUp();
    }

    /**
     * @test
     * @dataProvider data
     */
    public function ItShouldReturnSameValuesWhenItCreateADrink(
        string $type,
        float $price,
        int $sugarNumber,
        bool $extraHot
    ){
        $drinkType = new DrinkType($type);
        $drinkPrice = new DrinkPrice($price, $type);
        $drinkSugar = new DrinkSugar($sugarNumber);
        $drinkExtraHot = new DrinkExtraHot($extraHot);

        $drink = Drink::order($drinkType, $drinkPrice, $drinkSugar, $drinkExtraHot);

        $this->assertEquals($price, $drink->price()->value());
        $this->assertEquals($type, $drink->type()->value());
    }

    public function data(): array
    {
        return DrinkAsset::fixtures();
    }
}
