<?php

namespace GetWith\CoffeeMachine\Tests\Unit\CoffeeMachine\Drink\Domain\ValueObject;

use GetWith\CoffeeMachine\CoffeeMachine\Drink\Domain\Exception\DrinkSugarException;
use GetWith\CoffeeMachine\CoffeeMachine\Drink\Domain\ValueObject\DrinkSugar;
use PHPUnit\Framework\TestCase;

class DrinkSugarTest extends TestCase
{
    /**
     * @test
     */
    public function ItShouldReturnAnErrorWhenSugarNumberExceeded()
    {
        $this->expectException(DrinkSugarException::class);

        $drinkSugar = new DrinkSugar(3);
    }
}
