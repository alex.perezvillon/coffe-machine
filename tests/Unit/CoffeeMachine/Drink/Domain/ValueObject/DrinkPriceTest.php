<?php

namespace GetWith\CoffeeMachine\Tests\Unit\CoffeeMachine\Drink\Domain\ValueObject;

use GetWith\CoffeeMachine\CoffeeMachine\Drink\Domain\Exception\DrinkPriceException;
use GetWith\CoffeeMachine\CoffeeMachine\Drink\Domain\ValueObject\DrinkPrice;
use GetWith\CoffeeMachine\Tests\Assets\Drink\DrinkAsset;
use PHPUnit\Framework\TestCase;

class DrinkPriceTest extends TestCase
{
    /**
     * @test
     * @dataProvider data
     */
    public function ItShouldReturnAnErrorWhenDrinkMoneyIsNotEnough(string $type, $price)
    {
        $this->expectException(DrinkPriceException::class);

        $drinkPrice= new DrinkPrice(0, $type);
    }

    public function data(): array
    {
        return DrinkAsset::fixtures();
    }

}
