<?php

namespace GetWith\CoffeeMachine\Tests\Unit\CoffeeMachine\Drink\Domain\Service;

use GetWith\CoffeeMachine\CoffeeMachine\Drink\Domain\Drink;
use GetWith\CoffeeMachine\CoffeeMachine\Drink\Domain\Exception\DrinkTypeException;
use GetWith\CoffeeMachine\CoffeeMachine\Drink\Domain\ValueObject\DrinkExtraHot;
use GetWith\CoffeeMachine\CoffeeMachine\Drink\Domain\ValueObject\DrinkPrice;
use GetWith\CoffeeMachine\CoffeeMachine\Drink\Domain\ValueObject\DrinkSugar;
use GetWith\CoffeeMachine\CoffeeMachine\Drink\Domain\ValueObject\DrinkType;
use GetWith\CoffeeMachine\Tests\Assets\Drink\DrinkAsset;
use PHPUnit\Framework\TestCase;

class DrinkAvailableTest extends TestCase
{

    protected function setUp(): void
    {
        parent::setUp();
    }

    /**
     * @test
     * @TODO CREATE DrinkRepositoryMock
     */
    public function ItShouldReturnAnErrorWhenDrinkTypeAreNotInArray()
    {
        $this->assertTrue(true);
    }
}
