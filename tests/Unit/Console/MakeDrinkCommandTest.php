<?php

namespace GetWith\CoffeeMachine\Tests\Unit\Console;

use GetWith\CoffeeMachine\Console\MakeDrinkCommand;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Tester\CommandTester;

class MakeDrinkCommandTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        $application = new MakeDrinkCommand();
        $this->commandTester = new CommandTester($application);
    }

    /**
     * @test
     */
    public function ItShouldShowTheOrderCorrectlyWhenDrinkTypeIsTea()
    {
        //Arrange
        $options = [
            'drink-type' => 'tea',
            'money' => 0.4
        ];

        //Act
        $this->commandTester->execute($options);

        //Assertion
        $this->assertEquals('You have ordered a tea', trim($this->commandTester->getDisplay()));
    }

    /**
     * @test
     */
    public function ItShouldShowTheOrderCorrectlyWhenDrinkTypeIsCoffee()
    {
        //Arrange
        $options = [
            'drink-type' => 'coffee',
            'money' => 0.5
        ];

        //Act
        $this->commandTester->execute($options);

        //Assertion
        $this->assertEquals('You have ordered a coffee', trim($this->commandTester->getDisplay()));
    }

    /**
     * @test
     */
    public function ItShouldShowTheOrderCorrectlyWhenDrinkTypeIsChocolate()
    {
        //Arrange
        $options = [
            'drink-type' => 'chocolate',
            'money' => 0.6
        ];

        //Act
        $this->commandTester->execute($options);

        //Assertion
        $this->assertEquals('You have ordered a chocolate', trim($this->commandTester->getDisplay()));
    }

    /**
     * @test
     */
    public function ItShouldShowTheOrderCorrectlyWhenDrinkTypeIsTeaWith1Sugar()
    {
        //Arrange
        $options = [
            'drink-type' => 'tea',
            'money' => 0.4,
            'sugars' => 1,
        ];

        //Act
        $this->commandTester->execute($options);

        //Assertion
        $this->assertEquals('You have ordered a tea with 1 sugars (stick included)', trim($this->commandTester->getDisplay()));
    }

    /**
     * @test
     */
    public function ItShouldShowTheOrderCorrectlyWhenDrinkTypeIsCoffeeWith1Sugar()
    {
        //Arrange
        $options = [
            'drink-type' => 'coffee',
            'money' => 0.5,
            'sugars' => 1,
        ];

        //Act
        $this->commandTester->execute($options);

        //Assertion
        $this->assertEquals('You have ordered a coffee with 1 sugars (stick included)', trim($this->commandTester->getDisplay()));
    }

    /**
     * @test
     */
    public function ItShouldShowTheOrderCorrectlyWhenDrinkTypeIsChocolateWith1Sugar()
    {
        //Arrange
        $options = [
            'drink-type' => 'chocolate',
            'money' => 0.6,
            'sugars' => 1,
        ];

        //Act
        $this->commandTester->execute($options);

        //Assertion
        $this->assertEquals('You have ordered a chocolate with 1 sugars (stick included)', trim($this->commandTester->getDisplay()));
    }

    /**
     * @test
     */
    public function ItShouldShowTheOrderCorrectlyWhenDrinkTypeIsTeaExtraHot()
    {
        //Arrange
        $options = [
            'drink-type' => 'tea',
            'money' => 0.4,
            '--extra-hot' => true
        ];

        //Act
        $this->commandTester->execute($options);

        //Assertion
        $this->assertEquals('You have ordered a tea extra hot', trim($this->commandTester->getDisplay()));
    }

    /**
     * @test
     */
    public function ItShouldShowTheOrderCorrectlyWhenDrinkTypeIsCoffeeExtraHot()
    {
        //Arrange
        $options = [
            'drink-type' => 'coffee',
            'money' => 0.5,
            '--extra-hot' => true
        ];

        //Act
        $this->commandTester->execute($options);

        //Assertion
        $this->assertEquals('You have ordered a coffee extra hot', trim($this->commandTester->getDisplay()));
    }

    /**
     * @test
     */
    public function ItShouldShowTheOrderCorrectlyWhenDrinkTypeIsChocolateExtraHot()
    {
        //Arrange
        $options = [
            'drink-type' => 'chocolate',
            'money' => 0.6,
            '--extra-hot' => true
        ];

        //Act
        $this->commandTester->execute($options);

        //Assertion
        $this->assertEquals('You have ordered a chocolate extra hot', trim($this->commandTester->getDisplay()));
    }

    /**
     * @test
     */
    public function ItShouldShowTheOrderCorrectlyWhenDrinkTypeIsTeaExtraHotWith1Sugar()
    {
        //Arrange
        $options = [
            'drink-type' => 'tea',
            'money' => 0.4,
            'sugars' => 1,
            '--extra-hot' => true
        ];

        //Act
        $this->commandTester->execute($options);

        //Assertion
        $this->assertEquals('You have ordered a tea extra hot with 1 sugars (stick included)', trim($this->commandTester->getDisplay()));
    }

    /**
     * @test
     */
    public function ItShouldShowTheOrderCorrectlyWhenDrinkTypeIsCoffeeExtraHotWith1Sugar()
    {
        //Arrange
        $options = [
            'drink-type' => 'coffee',
            'money' => 0.6,
            'sugars' => 1,
            '--extra-hot' => true
        ];

        //Act
        $this->commandTester->execute($options);

        //Assertion
        $this->assertEquals('You have ordered a coffee extra hot with 1 sugars (stick included)', trim($this->commandTester->getDisplay()));
    }

    /**
     * @test
     */
    public function ItShouldShowTheOrderCorrectlyWhenDrinkTypeIsChocolateExtraHotWith1Sugar()
    {
        //Arrange
        $options = [
            'drink-type' => 'chocolate',
            'money' => 0.6,
            'sugars' => 1,
            '--extra-hot' => true
        ];

        //Act
        $this->commandTester->execute($options);

        //Assertion
        $this->assertEquals('You have ordered a chocolate extra hot with 1 sugars (stick included)', trim($this->commandTester->getDisplay()));
    }
}
