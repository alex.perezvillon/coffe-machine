<?php

namespace GetWith\CoffeeMachine\Tests\Assets\Drink;

use GetWith\CoffeeMachine\CoffeeMachine\Drink\Domain\ValueObject\DrinkType;

class DrinkAsset
{
    public static function fixtures(): array
    {
        return
            [
                [
                    DrinkType::TEA, 0.4, 0, true
                ],
                [
                    DrinkType::COFFEE, 0.5, 1, true
                ],
                [
                    DrinkType::CHOCOLATE, 0.6, 2, true
                ],
        ];
    }
}
