<?php

namespace GetWith\CoffeeMachine\Console;

use GetWith\CoffeeMachine\CoffeeMachine\Drink\Application\UseCase\OrderDrink\OrderDrinkUseCase;
use GetWith\CoffeeMachine\CoffeeMachine\Drink\Application\UseCase\OrderDrink\Response\OrderDrinkResponseConverter;
use GetWith\CoffeeMachine\CoffeeMachine\Drink\Domain\DrinkFactory;
use GetWith\CoffeeMachine\CoffeeMachine\Drink\Domain\Service\DrinkAvailable;
use GetWith\CoffeeMachine\CoffeeMachine\Drink\Infrastructure\Controller\DrinkController;
use GetWith\CoffeeMachine\CoffeeMachine\Drink\Infrastructure\Persistence\DrinkRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class MakeDrinkCommand extends Command
{
    protected static $defaultName = 'app:order-drink';

    public function __construct()
    {
        parent::__construct(MakeDrinkCommand::$defaultName);
    }


    protected function configure(): void
    {
        $this->addArgument(
            'drink-type',
            InputArgument::REQUIRED,
            'The type of the drink. (Tea, Coffee or Chocolate)'
        );

        $this->addArgument(
            'money',
            InputArgument::REQUIRED,
            'The amount of money given by the user'
        );

        $this->addArgument(
            'sugars',
            InputArgument::OPTIONAL,
            'The number of sugars you want. (0, 1, 2)',
            0
        );

        $this->addOption(
            'extra-hot',
            'e',
            InputOption::VALUE_NONE,
            $description = 'If the user wants to make the drink extra hot'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $drinkType = (string) strtolower($input->getArgument('drink-type'));
        $money = (float) $input->getArgument('money');
        $sugars = (int) $input->getArgument('sugars');
        $extraHot = (bool) $input->getOption('extra-hot');

        $repository = new DrinkRepository();
        $drinkAvailable = new DrinkAvailable($repository);
        $converter = new OrderDrinkResponseConverter();
        $factory = new DrinkFactory();
        $useCase = new OrderDrinkUseCase($factory,$drinkAvailable, $converter);
        $controller = new DrinkController($useCase);

        $data = $controller->execute($drinkType, $money, $sugars, $extraHot);

        $output->writeln($data->getContent());

        return 0;
    }
}
