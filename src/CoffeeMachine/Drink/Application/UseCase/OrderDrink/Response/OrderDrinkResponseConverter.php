<?php

namespace GetWith\CoffeeMachine\CoffeeMachine\Drink\Application\UseCase\OrderDrink\Response;

use GetWith\CoffeeMachine\CoffeeMachine\Drink\Domain\Drink;

final class OrderDrinkResponseConverter
{
    public function convert(Drink $drink): OrderDrinkResponse
    {
        $message = $drink->type()->message() . $drink->extraHot()->message() . $drink->sugar()->message();

        return new OrderDrinkResponse(
            $message
        );
    }
}