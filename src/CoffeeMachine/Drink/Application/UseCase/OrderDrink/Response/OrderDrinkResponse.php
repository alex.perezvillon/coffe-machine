<?php

namespace GetWith\CoffeeMachine\CoffeeMachine\Drink\Application\UseCase\OrderDrink\Response;


final class OrderDrinkResponse
{
    public string $message;

    public function __construct(string $message)
    {
        $this->message = $message;
    }
}