<?php

namespace GetWith\CoffeeMachine\CoffeeMachine\Drink\Application\UseCase\OrderDrink;

use GetWith\CoffeeMachine\CoffeeMachine\Drink\Application\UseCase\OrderDrink\Response\OrderDrinkResponse;
use GetWith\CoffeeMachine\CoffeeMachine\Drink\Application\UseCase\OrderDrink\Response\OrderDrinkResponseConverter;
use GetWith\CoffeeMachine\CoffeeMachine\Drink\Domain\DrinkFactory;
use GetWith\CoffeeMachine\CoffeeMachine\Drink\Domain\Service\DrinkAvailable;

final class OrderDrinkUseCase
{
    public function __construct(
        private DrinkFactory $drinkFactory,
        private DrinkAvailable $drinkAvailable,
        private OrderDrinkResponseConverter $orderDrinkResponseConverter
    ) {
    }

    public function execute(OrderDrinkRequest $request): OrderDrinkResponse
    {
        $drink = $this->drinkFactory->create(
            $request->drinkType(),
            $request->money(),
            $request->sugars(),
            $request->extraHot()
        );

        $this->drinkAvailable->exist($request->drinkType());

        return $this->orderDrinkResponseConverter->convert($drink);
    }
}