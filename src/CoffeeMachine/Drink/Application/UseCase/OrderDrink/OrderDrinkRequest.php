<?php

namespace GetWith\CoffeeMachine\CoffeeMachine\Drink\Application\UseCase\OrderDrink;

final class OrderDrinkRequest
{
    private string $drinkType;
    private float $money;
    private int $sugars;
    private float $extraHot;

    private function __construct(
        string $drinkType,
        float $money,
        int $sugars,
        float $extraHot
    ) {
        $this->drinkType = $drinkType;
        $this->money = $money;
        $this->sugars = $sugars;
        $this->extraHot = $extraHot;
    }

    public static function create(
        string $drinkType,
        float $money,
        int $sugars,
        bool $extraHot
    ): self
    {
        return new self($drinkType, $money, $sugars,  $extraHot);
    }

    public function drinkType() : string
    {
        return $this->drinkType;
    }

    public function money() : float
    {
        return $this->money;
    }

    public function sugars() : int
    {
        return $this->sugars;
    }

    public function extraHot() : bool
    {
        return $this->extraHot;
    }
}