<?php

namespace GetWith\CoffeeMachine\CoffeeMachine\Drink\Infrastructure\Persistence;

use GetWith\CoffeeMachine\CoffeeMachine\Drink\Domain\DrinkRepositoryInterface;
use GetWith\CoffeeMachine\CoffeeMachine\Drink\Domain\ValueObject\DrinkType;


final class DrinkRepository  implements DrinkRepositoryInterface
{
    //@TODO CREATE DRINK TABLE
    const TYPES = [
        DrinkType::TEA,
        DrinkType::COFFEE,
        DrinkType::CHOCOLATE
    ];

    public function __construct()
    {

    }

    public function findByType(string $type) : bool
    {
        return in_array($type, self::TYPES);
    }
}