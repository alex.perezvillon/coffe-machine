<?php

namespace GetWith\CoffeeMachine\CoffeeMachine\Drink\Infrastructure\Controller;

use GetWith\CoffeeMachine\CoffeeMachine\Drink\Application\UseCase\OrderDrink\OrderDrinkRequest;
use GetWith\CoffeeMachine\CoffeeMachine\Drink\Application\UseCase\OrderDrink\OrderDrinkUseCase;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

final class DrinkController
{

    public function __construct(private OrderDrinkUseCase $orderDrinkUseCase) {}

    public function execute(string $drinkType, float $money, int $sugars, bool $extraHot) : Response
    {
        try {
            $response = $this->orderDrinkUseCase->execute(OrderDrinkRequest::create(
                $drinkType,
                $money,
                $sugars,
                $extraHot
            ));
        } catch (\Exception $exception){
            return new Response($exception->getMessage());
        }

        return new Response($response->message);
    }
}