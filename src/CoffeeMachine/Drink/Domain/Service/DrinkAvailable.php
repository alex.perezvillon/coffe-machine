<?php

namespace GetWith\CoffeeMachine\CoffeeMachine\Drink\Domain\Service;

use GetWith\CoffeeMachine\CoffeeMachine\Drink\Domain\DrinkRepositoryInterface;
use GetWith\CoffeeMachine\CoffeeMachine\Drink\Domain\Exception\DrinkTypeException;

class DrinkAvailable
{
    public function __construct( private DrinkRepositoryInterface $drinkRepository)
    {
    }

    public function exist(string $type): void
    {
        $drink = $this->drinkRepository->findByType($type);

        if (!$drink) {
            throw DrinkTypeException::drinkNotAvailable();
        }
    }
}