<?php

namespace GetWith\CoffeeMachine\CoffeeMachine\Drink\Domain\ValueObject;

use GetWith\CoffeeMachine\CoffeeMachine\Drink\Domain\Exception\DrinkSugarException;

class DrinkSugar
{
    const SUGAR_NUMBER_MIN = 0;
    const SUGAR_NUMBER_MAX = 2;

    private int $value = self::SUGAR_NUMBER_MIN;

    public function __construct(int $sugarNumber)
    {
        if ($sugarNumber < self::SUGAR_NUMBER_MIN || $sugarNumber > self::SUGAR_NUMBER_MAX) {
            throw DrinkSugarException::sugarNumberExceeded();
        }

        $this->value = $sugarNumber;
    }

    public function value(): string
    {
        return $this->value;
    }

    public function message(): string
    {
        if ($this->value() > self::SUGAR_NUMBER_MIN) {
            return ' with ' . $this->value() . ' sugars (stick included)';
        }

        return '';
    }
}