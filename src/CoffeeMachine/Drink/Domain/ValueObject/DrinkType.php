<?php


namespace GetWith\CoffeeMachine\CoffeeMachine\Drink\Domain\ValueObject;


use GetWith\CoffeeMachine\CoffeeMachine\Drink\Domain\Exception\DrinkTypeException;

class DrinkType
{
    const TEA = 'tea';
    const COFFEE = 'coffee';
    const CHOCOLATE = 'chocolate';

    private string $value;

    public function __construct(string $drinkType)
    {
        $this->value = $drinkType;
    }

    public function value(): string
    {
        return $this->value;
    }

    public function message(): string
    {
        return 'You have ordered a ' . $this->value();
    }
}