<?php


namespace GetWith\CoffeeMachine\CoffeeMachine\Drink\Domain\ValueObject;


use GetWith\CoffeeMachine\CoffeeMachine\Drink\Domain\Exception\DrinkPriceException;

class DrinkPrice
{
    const TEA_PRICE = 0.4;
    const COFFEE_PRICE = 0.5;
    const CHOCOLATE_PRICE = 0.6;

    private float $value;

    public function __construct(float $price, string $drinkType)
    {
        //@TODO CREATE DOMAIN SERVICE
        if ($drinkType === DrinkType::TEA && $price < self::TEA_PRICE) {
            throw DrinkPriceException::insufficientMoney(DrinkType::TEA, self::TEA_PRICE);
        }

        if ($drinkType === DrinkType::COFFEE && $price < self::COFFEE_PRICE) {
            throw DrinkPriceException::insufficientMoney(DrinkType::COFFEE, self::COFFEE_PRICE);
        }

        if ($drinkType === DrinkType::CHOCOLATE && $price < self::CHOCOLATE_PRICE) {
            throw DrinkPriceException::insufficientMoney(DrinkType::CHOCOLATE, self::CHOCOLATE_PRICE);
        }

        $this->value = $price;
    }

    public function value(): float
    {
        return $this->value;
    }
}