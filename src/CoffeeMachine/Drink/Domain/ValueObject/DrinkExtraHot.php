<?php

namespace GetWith\CoffeeMachine\CoffeeMachine\Drink\Domain\ValueObject;

use GetWith\CoffeeMachine\CoffeeMachine\Drink\Domain\Exception\DrinkSugarException;

class DrinkExtraHot
{
    private bool $value;

    public function __construct(bool $value)
    {
        $this->value = $value;
    }

    public function value(): bool
    {
        return $this->value;
    }

    public function message(): string
    {
        if ($this->value()) {
            return ' extra hot';
        }

        return '';
    }
}