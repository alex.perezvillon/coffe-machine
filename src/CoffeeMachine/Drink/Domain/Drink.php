<?php

namespace GetWith\CoffeeMachine\CoffeeMachine\Drink\Domain;

use GetWith\CoffeeMachine\CoffeeMachine\Drink\Domain\ValueObject\DrinkExtraHot;
use GetWith\CoffeeMachine\CoffeeMachine\Drink\Domain\ValueObject\DrinkPrice;
use GetWith\CoffeeMachine\CoffeeMachine\Drink\Domain\ValueObject\DrinkSugar;
use GetWith\CoffeeMachine\CoffeeMachine\Drink\Domain\ValueObject\DrinkType;

final class Drink
{
    private DrinkType $type;
    private DrinkPrice $price;
    private DrinkSugar $sugar;
    private DrinkExtraHot $extraHot;

    private function __construct(
        DrinkType $type,
        DrinkPrice $price,
        DrinkSugar $sugar,
        DrinkExtraHot $extraHot,
    ) {
        $this->type = $type;
        $this->price = $price;
        $this->sugar = $sugar;
        $this->extraHot = $extraHot;
    }

    public static function order(
        DrinkType $type,
        DrinkPrice $price,
        DrinkSugar $sugar,
        DrinkExtraHot $extraHot
    ): self {
        return new self(
            $type,
            $price,
            $sugar,
            $extraHot
        );
    }

    public function type(): DrinkType
    {
        return $this->type;
    }

    public function price(): DrinkPrice
    {
        return $this->price;
    }

    public function sugar(): DrinkSugar
    {
        return $this->sugar;
    }

    public function extraHot(): DrinkExtraHot
    {
        return $this->extraHot;
    }
}