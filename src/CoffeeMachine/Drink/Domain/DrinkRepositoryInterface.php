<?php

namespace GetWith\CoffeeMachine\CoffeeMachine\Drink\Domain;

interface DrinkRepositoryInterface
{
    public function findByType(string $type) : bool;
}