<?php

namespace GetWith\CoffeeMachine\CoffeeMachine\Drink\Domain;

use GetWith\CoffeeMachine\CoffeeMachine\Drink\Domain\ValueObject\DrinkExtraHot;
use GetWith\CoffeeMachine\CoffeeMachine\Drink\Domain\ValueObject\DrinkPrice;
use GetWith\CoffeeMachine\CoffeeMachine\Drink\Domain\ValueObject\DrinkSugar;
use GetWith\CoffeeMachine\CoffeeMachine\Drink\Domain\ValueObject\DrinkType;

class DrinkFactory
{
    public  function create(
        string $type,
        float $price,
        int $sugar,
        bool $extraHot,
    ): Drink {
        $drinkType = new DrinkType($type);
        $drinkPrice = new DrinkPrice($price, $type);
        $drinkSugar = new DrinkSugar($sugar);
        $extraHot = new DrinkExtraHot($extraHot);

        return Drink::order(
            $drinkType,
            $drinkPrice,
            $drinkSugar,
            $extraHot
        );
    }
}