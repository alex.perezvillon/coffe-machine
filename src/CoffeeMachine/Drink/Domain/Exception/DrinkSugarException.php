<?php


namespace GetWith\CoffeeMachine\CoffeeMachine\Drink\Domain\Exception;


class DrinkSugarException extends \Exception
{
    public function __construct($message = "", $code = 0, \Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public static function sugarNumberExceeded(): self
    {
        return new self('The number of sugars should be between 0 and 2.', 0);
    }
}