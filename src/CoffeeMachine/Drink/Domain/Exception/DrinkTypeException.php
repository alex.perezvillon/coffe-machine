<?php


namespace GetWith\CoffeeMachine\CoffeeMachine\Drink\Domain\Exception;


class DrinkTypeException extends \Exception
{
    public function __construct($message = "", $code = 0, \Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public static function drinkNotAvailable(): self
    {
        return new self('The drink type should be tea, coffee or chocolate.', 0);
    }
}