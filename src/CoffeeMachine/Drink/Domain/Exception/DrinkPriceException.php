<?php


namespace GetWith\CoffeeMachine\CoffeeMachine\Drink\Domain\Exception;


class DrinkPriceException extends \Exception
{
    public function __construct($message = "", $code = 0, \Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public static function insufficientMoney(string $drinkType, float $price): self
    {
        return new self("The {$drinkType} costs {$price}.", 0);
    }
}