## Project set up

Install and run the application.
```
docker/composer install
docker/build
```

Examples of the use of the application.
```
docker/console app:order-drink tea 0.5 1 -e
docker/console app:order-drink coffee 0.5
docker/console app:order-drink chocolate 1 --extra-hot
```

Run tests
```
docker/test
```